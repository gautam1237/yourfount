class UsersController < ApplicationController
  def index
    if user_signed_in?
      @my_articles=current_user.articles.all
      @all_articles=Article.all
      @user=current_user
      @profile=@user.profile
      @blogger_account=@user.blogger_account
      @wordpress_account=@user.wordpress_account
      a=Engine.new
      @suggested_articles=[]						#to confirm --- suggested_articles is not nil 
      @suggested_articles=a.recommend_articles(current_user,50)
      if params[:skip]
        session[:skip]=1
      end
    end
  end
  def show
    @user=User.find(params[:id])
    @profile=@user.profile
  end

  def new
    @user=current_user
  end
  
  def update
    params[:user][:profile][:interests]= params[:user][:profile][:interests].delete(' ')
    params[:user][:profile][:interests]= params[:user][:profile][:interests].split(',')
    params[:user][:profile][:interests]= params[:user][:profile][:interests].uniq 			#this all is changing tags , string to array
=begin This parameters have to be added later
    params[:user][:profile][:employer]= params[:user][:profile][:employer].delete(' ')
    params[:user][:profile][:employer]= params[:user][:profile][:employer].split(',')
    params[:user][:profile][:employer]= params[:user][:profile][:employer].uniq 

    params[:user][:profile][:school]= params[:user][:profile][:school].delete(' ')
    params[:user][:profile][:school]= params[:user][:profile][:school].split(',')
    params[:user][:profile][:school]= params[:user][:profile][:school].uniq 

    params[:user][:profile][:undergrad_university]= params[:user][:profile][:undergrad_university].delete(' ')
    params[:user][:profile][:undergrad_university]= params[:user][:profile][:undergrad_university].split(',')
    params[:user][:profile][:undergrad_university]= params[:user][:profile][:undergrad_university].uniq     

    params[:user][:profile][:postgrad_university]= params[:user][:profile][:postgrad_university].delete(' ')
    params[:user][:profile][:postgrad_university]= params[:user][:profile][:postgrad_university].split(',')
    params[:user][:profile][:postgrad_university]= params[:user][:profile][:postgrad_university].uniq  
    session[:skip]=1
=end

    @user=current_user
    respond_to do |format|                                                                                                         #check if profile already exists
      @profile= @user.profile
      if @profile.update_attributes(params[:user][:profile])                                             #update attributes
        session[:skip]=1
        format.html {redirect_to root_path, :notice => "Profile Updated"}
      else
        format.html {render :action=>"edit", :notice=> "Unable to update profile , sorry! :("}
      end
    end
  end

  def edit
    if user_signed_in? && current_user==User.find(params[:id])
      @user=current_user
      @profile=@user.profile
      @profile.interests=@profile.interests.join(',').to_a                             #convert interests (array) into string
      @profile.employer=@profile.employer.join(',').to_a
      @profile.school=@profile.school.join(',').to_a
      @profile.undergrad_university=@profile.undergrad_university.join(',').to_a
      @profile.postgrad_university=@profile.postgrad_university.join(',').to_a
    elsif user_signed_in? && current_user!=User.find(params[:id])
      redirect_to user_path(params[:id]), :notice => "You can't edit other user's profile"
    else 
      redirect_to new_user_session_path, :notice => "Sign in to edit your profile"
    end
  end

  def top_articles(user, number)
    @top=Article.any_in(:tags  => user.profile.interests)
     if @top.length<=number.to_i
       return @top
     else
       @top=@top.order_by([:title, :asc])
       return @top
     end
  end

  def add_interests
    if user_signed_in?
      @user=User.find(params[:id])
      @profile=@user.profile
      interests=params[:interests]
      interests=interests.split(",")
      @profile.interests=@profile.interests+interests
      if @profile.save
        redirect_to root_path , :notice => "You seem like a interesting person! ;)"
      else
        redirect_to root_path , :notice => "Ops! Can't save your interests, please try again or report us."
      end  
    end    
  end
end
