class WordpressAccountsController < ApplicationController
	respond_to :html, :json, :js
  def new
        if user_signed_in?
        	@user=current_user
        	@wordpress_account=@user.build_wordpress_account
        else
          redirect_to users_path, :notice=>"Login to Sync"
        end
  end

  def create
  	if user_signed_in? 
	  	@user=current_user
			wordpress_account=@user.build_wordpress_account
			wordpress_account.blog=params[:wordpress_account][:blog]
			wordpress_account.username=params[:wordpress_account][:username]
			wordpress_account.password=params[:wordpress_account][:password]
			synchronize(wordpress_account)
		end
  end

  def show
	if ( not user_signed_in? ) or current_user.wordpress_account.nil? 
		redirect_to root_path , :notice=>'login first'
	else
		@user=User.find(params[:user_id])
		@wordpress_account=WordpressAccount.find(params[:id])
	end
  end

  def index
  	@user=User.find(params[:user_id])
  	@wordpress_account=@user.wordpress_account
  end


  def assert_url(url)
  	if url[0,5]=="https" || url[0,4] =="http"
  		return false
  	else
  		return true
  	end
  end


  def synchronize(*args)
  	if args.length==0
	  	@user=User.find(params[:user_id])
	  	@wordpress_account=WordpressAccount.find(params[:id])
	  	if user_signed_in? && @user=current_user && @wordpress_account.user=current_user

	  		synchronize(@wordpress_account)
	  	end
  	elsif args.length==1
  		wordpress_account=args[0]
  		@user=current_user
	  	begin
				if assert_url(wordpress_account.blog)
					blog = Wordpressto::WordpressBlog.new :url => "https://"+wordpress_account.blog+'/xmlrpc.php',:username => wordpress_account.username, :password=> wordpress_account.password
				else
					raise "Please make sure you enter correct address of your blog without http or https"
				end
				posts=blog.posts.find_recent(:limit => 100)
			rescue => e
				redirect_to new_user_wordpress_account_path(@user) ,:notice=>e.message
			else
				@user=current_user
				@wordpress_account=wordpress_account

				@wordpress_account.clash=Array.new

				for post in posts
					article=@user.articles.build
					article.content=post.description
					article.title=post.title
					article.tags=post.keywords
					article.from=1
					article.hidden_tags=Hash.new
					if @user.articles.where(:title=>article.title).empty?
						if not article.save	
							temp_hash=Hash.new
							temp_hash['title']=post.title
							temp_hash['content']=post.description
							temp_hash['tags']=post.keywords.map { |keyword|  keyword.downcase}
							temp_hash['owner']=@user.username
							@wordpress_account.clash << temp_hash
						end
					end			
				end
				if @wordpress_account.save
					redirect_to users_path, :notice=>'Articles imported'
				else
					redirect_to users_path , :notice=>'Unable to create Wordpress Account'
				end
			end
		end
  end

  def destroy
  	@user=User.find(params[:user_id])
  	@wordpress_account=WordpressAccount.find(params[:id])
  	if user_signed_in? && @user=current_user && @wordpress_account.user=current_user
  		@user.articles.where(:from=>1).each{ |article|
  			article.delete
  		}
  		if @wordpress_account.destroy
  			redirect_to user_path(@user), :notice => "Wordpress Account deleted"
  		else
  			redirect_to user_wordpress_account_path(@user,@wordpress_account), :notice => "Unable to delete Wordpress Account"
  		end
  	else
  		if user_signed_in?
  			redirect_to user_path(@user), :notice => "Not your account"
  		else
  			redirect_to new_user_session_path, :notice => "Login to delete your account"
  		end
  	end
  	
  end
end