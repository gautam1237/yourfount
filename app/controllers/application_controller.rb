class ApplicationController < ActionController::Base
  protect_from_forgery
  helper :all
  helper_method :current_user_profile

  def current_user_profile
    if user_signed_in?
    @current_user_profile=current_user.profile
    end
    @current_user_profile=nil
  end

end
