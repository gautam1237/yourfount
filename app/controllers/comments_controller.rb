class CommentsController < ApplicationController
  def index
    @comments= Article.find(params[:article_id]).comments.all
  end
  def new
   if user_signed_in?
      @article= Article.find(params[:article_id])
      @comment= @article.comments.build
    else
      respond_to do |format|
        format.html {redirect_to new_user_session_path }
      end
    end

  end
  def update
    if user_signed_in?
      @comment=Comment.find(params[:comment]["comment_id"])
      if params[:unlike] 
	@comment.comment_likes.delete current_user.email				#undislike-unlike-unspam
      end
      if  params[:undislike] 
	@comment.comment_dislikes.delete current_user.email
      end
      if  params[:unspam]
	@comment.comment_spams.delete current_user.email	
      end
      if params[:like]
        @comment.comment_likes << current_user.email
      elsif params[:dislike]
        @comment.comment_dislikes << current_user.email
      elsif params[:spam]
        @comment.comment_spams << current_user.email
      end
      @comment.save
      redirect_to Article.find(params[:article_id])
    else
      respond_to do |format|
        format.html {redirect_to new_user_session_path, :notice=>"Sign-in to update the comment." }
      end
    end	
  end

  def show
  end
  
  def edit
  end

  def create
    if user_signed_in?
      @article= Article.find(params[:article_id])
      params[:comment][:by]=current_user						#inserting owner of the comments
      @comment=@article.comments.build(params[:comment])
      respond_to do |format|
       if ( not @comment.data.empty? ) and ( @comment.data.gsub(/\s+/, "").length!=0)
          if @comment.save
            format.html { redirect_to @article,:notice=> "Commented"}
          else
            format.html { render :action=>"new", :notice=> "Not able to comment, sorry :("}
          end
       else
          format.html { redirect_to @article,:notice=> "Comment empty"}
       end
      end
    else
      respond_to do |format|
        format.html {redirect_to new_user_session_path, :notice=>"Sign-in to post your comment." }
      end
    end
  end
  def destroy
    @article=Article.find(params[:article_id])
    @comment=Comment.find(params[:id])
    respond_to do |format|
      if @comment.destroy
        format.html {redirect_to @article, :notice => "Comment Deleted"}
      else
        format.html {redirect_to @article, :notice => "Not able to delete comment"}
      end
    end
    
  end
end
