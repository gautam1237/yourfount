class ArticlesController < ApplicationController


	def edit
		if user_signed_in? && current_user==Article.find(params[:id]).owner
			@article=Article.find(params[:id])
  		elsif user_signed_in? && current_user!=Article.find(params[:id]).owner
  			redirect_to article_path(params[:id]), :notice => "You can edit only your articles!!"
  		else
  			redirect_to new_user_session_path, :notice => "Sign in to edit your article"
  		end
	end

	def update
		if user_signed_in?
			@article=Article.find(params[:id])
			@user=current_user
			engine=Engine.new
			if params[:like]
				@article.liked_by << @user.id
				factor=5
				engine.normalize(@user,@article,factor)
			end
			if params[:unlike]
				@article.liked_by.delete @user.id
				factor=-5
				engine.normalize(@user,@article,factor)
			end
			if params[:dislike]
				@article.disliked_by << @user.id
				factor=-3
				engine.normalize(@user,@article,factor)
			end
			if params[:undislike]
				@article.disliked_by.delete @user.id
				factor=3
				engine.normalize(@user,@article,factor)
			end
			if params[:spam]
				@article.spamed_by << @user.id
			end
			if params[:unspam]
				@article.spamed_by.delete @user.id
			end
			if params[:article]
				params[:article][:tags]=convert_string_to_array_with_downcase(params[:article][:tags])
			end
	  	respond_to do |format|
	  		if @article.update_attributes(params[:article])
	  			format.html {redirect_to @article, :notice => "Article Updated"}
	  		else
	  			format.html {render :action=>"edit", :notice=> "Unable to update Article , sorry! :("}
	  		end
	  	end
	  else
	  	@article=Article.find(params[:id])
	  	respond_to do |format|
	  		format.html { redirect_to new_user_session_path, :notice => "Sign-in to update the article ."}
	  	end
	  end
	end


	def show
		@article=Article.find(params[:id])
		@user=@article.owner
		@profile=@user.profile
		@comments=@article.comments.all
		@comment=@article.comments.build
	end

	def new
		if user_signed_in?
			@article =current_user.articles.build
		else
			redirect_to articles_path, :notice=>"Login to create Article"
		end
	end


	def create
		if user_signed_in?
				params[:article][:tags]=convert_string_to_array_with_downcase(params[:article][:tags])
	      @article= current_user.articles.build(params[:article])
	      respond_to do |format|
	      	if @article.save
	      		format.html {redirect_to @article, :notice => "Article Created"}
	      	else
	      		format.html {render :action=>"new", :notice=> "Unable to create Article , sorry! :("}
	      	end
	      end
	  	end
	end


	def index
		@temp=Article.all
		a=Engine.new
	    @suggested_articles=[]						#to confirm --- suggested_articles is not nil
	    if user_signed_in?
	    	@suggested_articles=a.recommend_articles(current_user,3)
	    end
	end


	def destroy
		@article=Article.find(params[:id])
		respond_to do |format|
			if @article.destroy
				format.html {redirect_to root_path, :notice => "Article Deleted"}
			else
				format.html {render :action=>"show", :notice=> "Unable to delete Article , sorry! :("}
			end
		end
	end

	def convert_string_to_array_with_downcase(string)
		return string.split(",").map{|tag| tag.downcase}
	end

end

