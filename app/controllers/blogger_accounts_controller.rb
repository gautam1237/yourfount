require 'blogger'
class BloggerAccountsController < ApplicationController

  def show
		if ( not user_signed_in? ) or current_user.blogger_account.nil? 
			redirect_to root_path , :notice=>'login first'
		else
			@user=User.find(params[:user_id])
			@blogger_account=BloggerAccount.find(params[:id])
		end
  end

  def new
	  if user_signed_in?
	    @user=current_user
	    @blogger_account=@user.build_blogger_account
	  else
	    redirect_to users_path, :notice=>"Login to Sync"
	  end
  end

  def create
  	if user_signed_in?
	  	@user=current_user
			blogger_account=@user.build_blogger_account
			blogger_account.username=params[:blogger_account][:username]
			blogger_account.password=params[:blogger_account][:password]
			synchronize(blogger_account)
		end
  end

  def synchronize(*args)
  	if args.length==0
  		@user=User.find(params[:user_id])
	  	@blogger_account=BloggerAccount.find(params[:id])
	  	if user_signed_in? && @user=current_user && @blogger_account.user=current_user
	  		synchronize(@blogger_account)
	  	end
  	elsif args.length==1
  		blogger_account=args[0]
  		@user=current_user
			begin
				@account = Blogger::Account.new('default',blogger_account.username,blogger_account.password)
			rescue =>e
				redirect_to user_path(@user),:notice=>e.message
			else
				if @account.authenticated?
					@blogger_account=blogger_account
					@blogger_account.clash=Array.new
					current_user.blogger_account=@blogger_account
					blogs=@account.blogs
					for blog in blogs
						posts=blog.posts
							for post in posts
								article=@user.articles.build
								article.title=post.title
								article.content=post.content
								article.tags=[]
								article.from=2
								for  category in post.categories
									article.tags << category.downcase
								end
								article.hidden_tags=Hash.new
								article.owner=current_user
								if @user.articles.where(:title=>article.title).empty?
									if not article.save
										temp_hash=Hash.new
										temp_hash['title']=article.title
										temp_hash['content']=article.content
										temp_hash['tags']=article.tags
										temp_hash['owner']=article.owner.username
										@blogger_account.clash << temp_hash
									end
								end
							end
						end
						if @blogger_account.save
							redirect_to root_path,:notice=>'Articles imported'
						else
							redirect_to root_path,:notice=>'Unable to create Blogger Account'
						end
				else
					redirect_to root_path,:notice=>'username password do not match'		
				end
	    end
  	end
  end

  def destroy
  	@user=User.find(params[:user_id])
  	@blogger_account=BloggerAccount.find(params[:id])
  	if user_signed_in? && @user=current_user && @blogger_account.user=current_user
  		@user.articles.where(:from=>2).each{ |article|
  			article.delete
  		}
  		if @blogger_account.destroy
  			redirect_to user_path(@user), :notice => "Blogger Account deleted"
  		else
  			redirect_to user_blogger_account_path(@user,@blogger_account), :notice => "Unable to delete Blogger Account"
  		end
  	else
  		if user_signed_in?
  			redirect_to user_path(@user), :notice => "Not your account"
  		else
  			redirect_to new_user_session_path, :notice => "Login to delete your account"
  		end
  	end
  end

end