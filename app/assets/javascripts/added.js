function create_profile(profile) // to expand the details of the particular person..
{
//this function has to expand the details section by pushing down the other contents of the page.

var htmls="<table width='100%' height='30px' style='font-family:arial;color:rgb(1, 1, 20);font-size:20px'>"
//alert(profile.first_name);
if(profile.first_name!="NULL" && profile.last_name!="NULL")
	htmls+="<tr><td>"+profile.first_name+" "+profile.last_name+"</td></tr>"
else if(profile.first_name!="NULL" )
	htmls+="<tr><td>"+profile.first_name+"</td></tr>"
else if(profile.last_name!="NULL" )
	htmls+="<tr><td>"+profile.last_name+"</td></tr>"
if(profile.sex!="NULL")
	htmls+="<tr><td>"+profile.sex+"</td></tr>"
if(profile.current_city!="NULL")
	htmls+="<tr><td>Current City: "+profile.current_city+"</td></tr>"
if(profile.hometown!="NULL")
	htmls+="<tr><td>Hometown: "+profile.hometown+"</td></tr>"
if(profile.dob!="NULL")
	htmls+="<tr><td>Date of Birth: "+profile.dob+"</td></tr>"
if(profile.punch_line!="NULL")
	htmls+="<tr><td>Punch Line: "+profile.punch_line+"</td></tr>"
if(profile.about!="NULL")
	htmls+="<tr><td>About: "+profile.about+"</td></tr>"
htmls+="</table>"
var detail = document.getElementById("details");
detail.style.width="100%";
detail.style.height="90%";
detail.innerHTML+=htmls;


} //function expand

function expand_profile(profile)
{	
	create_profile(profile);
	$('#gogogo2').hide();
	$('div.tableWrap').hide();
	$('#gogogo').click(function(){
		$('div.tableWrap').slideDown("slow");
		$('#gogogo').hide();
		$('#gogogo2').show();
			return false;
	});
	$('#gogogo2').click(function(){
		$('#gogogo2').hide();
		$('#gogogo').show();
		$('div.tableWrap').slideUp("slow");
			return false;
	});
}