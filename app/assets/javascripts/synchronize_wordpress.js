function synchronize_wordpress(user,wordpress_account)
{
	$.ajax({ url: "/users/"+user+"/wordpress_accounts/"+wordpress_account+"/synchronize",
		type: 'POST',
		beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
		success: function(response) {
			$.get ("/users/"+user+"/wordpress_accounts/"+wordpress_account+".js/");
		}
	});
}