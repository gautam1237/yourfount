function synchronize_blogger(user,blogger_account)
{
	$.ajax({ url: "/users/"+user+"/blogger_accounts/"+blogger_account+"/synchronize",
		type: 'POST',
		beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
		success: function(response) {
			$.get ("/users/"+user+"/blogger_accounts/"+blogger_account+".js/");
		}
	});
}