class User
  include Mongoid::Document

  after_create :init_func

  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable

  devise :database_authenticatable, :registerable,:confirmable,
         :recoverable, :rememberable, :trackable, :validatable  ,:authentication_keys => [:login]


## Database authenticatable
  field :email,              :type => String, :null => false
  field :encrypted_password, :type => String, :null => false

## Confirmable
   field :confirmation_token,   :type => String
   field :confirmed_at,         :type => Time
   field :confirmation_sent_at, :type => Time
  # field :unconfirmed_email,    :type => String # Only if using reconfirmable
  
## Recoverable
  field :reset_password_token,   :type => String
  field :reset_password_sent_at, :type => Time

## Rememberable
  field :remember_created_at, :type => Time

## Trackable
  field :sign_in_count,      :type => Integer
  field :current_sign_in_at, :type => Time
  field :last_sign_in_at,    :type => Time
  field :current_sign_in_ip, :type => String
  field :last_sign_in_ip,    :type => String


#User Field
  field :username ,          :type => String
  field :email,              :type => String

  has_many :comments , :inverse_of => :by
  has_many :articles, :inverse_of => :owner
  has_one :profile, :inverse_of=>:user
  has_one :blogger_account , :inverse_of=>:user
  has_one :wordpress_account , :inverse_of=>:user



  attr_accessor :login
  accepts_nested_attributes_for :profile

  validates :email, :confirmation => true
  validates_uniqueness_of :username 

  def self.find_for_database_authentication(conditions)
    login = conditions.delete(:login)
    self.any_of({ :username => login }, { :email => login }).first
  end

  def init_func
  	if self.profile.nil?
  	 self.profile=self.build_profile
  	end
  	
  	self.profile.save
  	self.save
  end

end