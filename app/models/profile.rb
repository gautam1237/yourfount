class Profile
  include Mongoid::Document
  field :first_name,    :type => String
  field :last_name,     :type => String
  field :interests ,    :type => Array ,        :default => []
  field :hidden_tags ,  :type => Hash  ,        :default => Hash.new
  field :sex,           :type => String
  field :current_city,  :type => String
  field :hometown,      :type => String
  field :dob,           :type => Date
  field :about,         :type => String
  field :punch_line ,   :type => String
  field :mobile_number, :type => String
  field :employer,      :type => Array ,        :default => []
  field :school,        :type => Array ,        :default => []
  field :undergrad_university, :type => Array , :default => []
  field :postgrad_university, :type => Array ,  :default => []
  field :religion,      :type => String
  field :politics,      :type => String
  belongs_to :user ,    :class_name=>"User"
end
