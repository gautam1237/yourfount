class Comment
  include Mongoid::Document
  include Mongoid::Timestamps
  field :data, :type => String
  field :comment_likes, :type => Array ,:default=>[]
  field :comment_dislikes, :type => Array, :default=>[]
  field :comment_spams, :type => Array, :default=>[]
  belongs_to :article , :class_name => "Article"
  belongs_to :by, :class_name=> "User"
end
