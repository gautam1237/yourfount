class WordpressAccount
  include Mongoid::Document
  field :blog , :type=>String
  field :username, :type=>String 
  field :password , :type=>String 
 
  field :clash, :type=>Array

  belongs_to :user , :class_name=>"User"
end
