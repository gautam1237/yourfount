class Article
  include Mongoid::Document
  include Mongoid::Timestamps
  require "json"
  field :title, :type => String
  field :content, :type => String

  field :tags , :type => Array ,:default=>[]

  field :liked_by , :type => Array , :default=>[]
  field :disliked_by , :type => Array , :default=>[]
  field :spamed_by , :type => Array , :default=>[]

  field :hidden_tags , :type => Hash , :default=>Hash.new
  field :from , :type => Integer, :default=>0   # 0 for yourfount, 1 for wordpress, 2 for blogger

  has_many :comments , :inverse_of => :article
  belongs_to :owner ,:class_name => "User"

  validates_presence_of :title
  validates_presence_of :content
  validates_presence_of :tags
end
