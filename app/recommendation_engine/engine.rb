require "log4r"

class Engine
  def possible_articles(user)                                                                                                 #finds all possible article that user can like
    logger=Logger.new(STDOUT)   
    all_articles=Article.all
    user_articles=user.articles.all
    already_liked_articles=Article.any_in(:liked_by=>[user.id])
    possible_articles=all_articles-user_articles-already_liked_articles
    return possible_articles
  end

  def similarity(user,article)

#  	puts	 "User : " + user.username + ", " + "Article : " + article.title
		probability=0			#final value

		article_hidden_tags_sum=article.hidden_tags.values.sum
		user_hidden_tags_sum=user.profile.hidden_tags.values.sum

		user_all_tags_sum=user_hidden_tags_sum+user.profile.interests.count*20
		article_all_tags_sum=article_hidden_tags_sum+article.tags.count*20

#		puts "User tags sum: " + user_all_tags_sum.to_s
#		puts "Article tags sum: " + article_all_tags_sum.to_s

#		art_sum=art_sum/100.0
#		use_sum=use_sum/100.0			#normalizing to 100

		article_tags_hash=Hash.new						#Hash for new normalized tags
		user_tags_hash=Hash.new								#Hash for new normalized tags

		article.hidden_tags.each do |key,value|		
			article_tags_hash[key]=(value/(article_all_tags_sum*1.0))*100
		end
#		puts "Article Hidden Tags : "
#		article.hidden_tags.each do |key,value|
#			puts key + ": " + value.to_s
#		end

		article.tags.each do |x|
			if article_tags_hash[x].nil?
				article_tags_hash[x]=0
			end
			article_tags_hash[x]=(article_tags_hash[x]+20/(article_all_tags_sum*1.0))*100
		end

		user.profile.hidden_tags.each do |key,value|
			user_tags_hash[key]=(value/(user_all_tags_sum*1.0))*100
		end

#		puts "User Hidden Tags : "
#		user.profile.hidden_tags.each do |key,value|
#			puts key + ": " + value.to_s
#		end
		user.profile.interests.each do |x|
			if user_tags_hash[x].nil?
				user_tags_hash[x]=0
			end
			user_tags_hash[x]=(user_tags_hash[x]+20/(user_all_tags_sum*1.0))*100
		end
#		puts "User Final Hash: "
#		user_tags_hash.each do |key,value|
#			puts key + ": " + value.to_s
#		end
#		puts "Article Final Hash: "
#		article_tags_hash.each do |key,value|
#			puts key + ": " + value.to_s
#		end

		article_tags_hash.each do |key,value|
			if not  user_tags_hash[key].nil?
#				puts "HERE?????????????????????????????????????????????????"
				if user_tags_hash[key]<article_tags_hash[key]
					probability=probability+user_tags_hash[key]
				else
					probability=probability+article_tags_hash[key]
#					puts "Probability now: " + probability.to_s
				end
			end		
		end	


		if article.liked_by.count > 0
			probability=probability*article.liked_by.count
		end

		p "Probability : " +probability.to_s	
	
		return probability

	
    #prob= ((user.profile.interests&article.tags).size)/((user.profile.interests|article.tags).size).to_f                 #number of matching-tags/number of all-tags
    #prob=prob.to_f*((article.article_likes.count+1)/(article.article_dislikes.count+1).to_f)
    #return prob
  end

  def sort_articles(user)
    articles=possible_articles(user)
    if articles.nil?
      return nil
    end
    probability=Hash.new
    articles.each do |article|
    	similarity_probability=similarity(user, article)
      if similarity_probability >0
      	probability[article]=similarity_probability
      end
    end
    array=probability.sort_by{|key,value| value}						#Ideally, this should not be done, this is reversed the order of probability as sort_by function sort in increasing values, and we want in decreasing order, but we are doing it this for now because javascript code reverse it again, so 2 times reverse of suggested articles by probability will show them in correct order
    sorted_articles=[]
    array.each do |pair|
      sorted_articles << pair[0]
    end
    return sorted_articles
  end

  def recommend_articles(user,num,interests=[])
  		articles= sort_articles(user)
    	if articles.nil?
      	return nil
    	else
      	return articles[0..num-1]
    	end
  end


  def normalize(user,article,factor)
		article_hidden_tags_sum=article.hidden_tags.values.sum
		user_hidden_tags_sum=user.profile.hidden_tags.values.sum

		user_all_tags_sum=user_hidden_tags_sum+user.profile.interests.count*20
		article_all_tags_sum=article_hidden_tags_sum+article.tags.count*20
				
		temp=user.profile.hidden_tags.clone
     
		if article_all_tags_sum>0
			article.hidden_tags.each do |key,value|
				if user.profile.hidden_tags[key].nil?
				  user.profile.hidden_tags[key]=0
				end
				user.profile.hidden_tags[key]=user.profile.hidden_tags[key]+((value*1.0)/article_all_tags_sum)*factor
			end
			article.tags.each do |key,value|
				if user.profile.hidden_tags[key].nil?
					user.profile.hidden_tags[key]=0
				end
				user.profile.hidden_tags[key]=user.profile.hidden_tags[key]+(20.0/article_all_tags_sum)*factor
			end
		end

		if user_all_tags_sum>0
			temp.each do |key,value|
				if article.hidden_tags[key].nil?
					article.hidden_tags[key]=0
				end
				article.hidden_tags[key]=article.hidden_tags[key]+((value*1.0)/user_all_tags_sum)*factor
			end
			user.profile.interests.each do |key,value|
				if article.hidden_tags[key].nil?
					article.hidden_tags[key]=0
				end
				article.hidden_tags[key]=article.hidden_tags[key]+(20.0/user_all_tags_sum)*factor
			end
		end
		user.save
		user.profile.save 
		article.save
	end

end