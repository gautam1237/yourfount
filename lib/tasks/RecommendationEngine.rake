namespace :RecommendationEngine do

  desc "a basic task"
  task :basic => :environment do
    puts "I'm a basic task"
  end

  desc "Task to check if Recommendation Engine gives correct output"
  task :check=>:environment do
  	engine=Engine.new
  	a=Article.all
  	u=User.where(:username=>"gautam").first
  	a.each{|i|
  		puts engine.similarity(u,i)
  	}
  end


  desc "RecommendArticles"
  task :recommend=>:environment do
  	engine=Engine.new
  	u=User.where(:username=>"gautam").first
  	rec=engine.recommend_articles(u,10)
  	rec.each{|art|
  		puts art.title
  	}
  end
end